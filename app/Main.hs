module Main where

import Options
import Regex
import Imports
import qualified Automata as FA

main :: IO ()
main = runCommand $ \args ->
  let eRegex = (runParser <=< runTokenizer defaultTokens) $ regex args
  in case eRegex of
    Left e -> print e
    Right re ->
      let dfa = FA.fromRegex re :: FA.DFA FA.ASCII Int
      in do
        ls <- lines <$> getContents
        mapM_ (gerpLine dfa) ls

gerpLine :: FA.DFA FA.ASCII Int -> String -> IO ()
gerpLine dfa line
  | match line = putStrLn line
  | otherwise = return ()
  where
  match :: String -> Bool
  match [] = False
  match ls = FA.accepts dfa (FA.pack ls) -- || match (tail ls)
