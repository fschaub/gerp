{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}


module Regex.Types
  ( Regex(..)
  , RegexTok(..)
  , Tokenizer(..)
  , Tokens(..)
  , Parser(..)
  ) where


import qualified Text.Parsec as Parsec
import qualified Text.Parsec.Char as Parsec


data Regex
  = Atom Char
  | Any
  | Asterate Regex
  | Not Regex
  | Sequence Regex Regex
  | Union Regex Regex
  deriving (Show)




data RegexTok
  = AtomTok !Char
  | AsterateTok
  | UnionTok
  | AnyTok
  | NotTok
  | ParenthesesOpenTok
  | ParenthesesCloseTok
  deriving (Show, Eq)

data Tokens = Tokens
  { escapeChar :: !Char
  , notsymbolChar :: !Char
  , asterateChar :: !Char
  , anysymbolChar :: !Char
  , unionChar :: !Char
  , parenthesesOpenChar :: !Char
  , parenthesesCloseChar :: !Char
  }

type Tokenizer a = Parsec.Parsec String () a
type Parser a = Parsec.Parsec [RegexTok] () a
