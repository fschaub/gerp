{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
module Regex.Tokenizer
  ( RegexTok(..)
  , defaultTokens
  , runTokenizer
  , matchAny
  , matchNot
  , matchAtom
  , matchAsterate
  , matchUnion
  , matchParenthesesOpen
  , matchParenthesesClose
  ) where

import Imports
import Regex.Types


import qualified Text.Parsec as Parsec
import qualified Text.Parsec.Char as Parsec
import qualified Text.Parsec.Prim as Parsec
import qualified Text.Parsec.Pos as Parsec



runTokenizer :: Tokens -> String -> Either String [RegexTok]
runTokenizer toks = fromError . Parsec.runParser (tokenizer toks) () "Tokenizer"
  where
    fromError (Left e) = Left $ show e
    fromError (Right x) = Right x

defaultTokens :: Tokens
defaultTokens = Tokens
  { escapeChar = '\\'
  , notsymbolChar = '^'
  , asterateChar = '*'
  , anysymbolChar = '.'
  , unionChar = '+'
  , parenthesesOpenChar = '('
  , parenthesesCloseChar = ')'
  }

tokenizer :: Tokens -> Tokenizer [RegexTok]
tokenizer toks = Parsec.many $ foldr1 (<|>)
  [ escape
  , asterate
  , anysymbol
  , notsymbol
  , union
  , parentheses
  , AtomTok <$> Parsec.anyChar
  ]
  where
    escape :: Tokenizer RegexTok
    escape = Parsec.try $ do
      _ <- Parsec.char $ escapeChar toks
      x <- Parsec.anyChar
      return . AtomTok $ case x of
        'n' -> '\n'
        't' -> '\t'
        _ -> x
    asterate :: Tokenizer RegexTok
    asterate = Parsec.try $ do
      _ <- Parsec.char $ asterateChar toks
      return AsterateTok
    anysymbol :: Tokenizer RegexTok
    anysymbol = Parsec.try $ Parsec.char (anysymbolChar toks) >> return AnyTok
    notsymbol :: Tokenizer RegexTok
    notsymbol = Parsec.try $ Parsec.char (notsymbolChar toks) >> return NotTok
    union :: Tokenizer RegexTok
    union = Parsec.try $ Parsec.char (unionChar toks) >> return UnionTok
    parentheses :: Tokenizer RegexTok
    parentheses = foldr1 (<|>)
      [ Parsec.try $ Parsec.char (parenthesesOpenChar toks) >> return ParenthesesOpenTok
      , Parsec.try $ Parsec.char (parenthesesCloseChar toks) >> return ParenthesesCloseTok
      ]





satisfyTok :: (RegexTok -> Bool) -> Parser RegexTok
satisfyTok f = Parsec.tokenPrim show updatePos test
  where
    -- updatePos :: Parsec.SourcePos -> RegexTok -> [RegexTok] -> Parsec.SourcePos
    updatePos p c cs = Parsec.updatePosString p $ show c
    test x = if f x then Just x else Nothing


matchTok :: RegexTok -> Parser RegexTok
matchTok t = satisfyTok (== t)

matchAtom :: Parser Char
matchAtom = satisfyTok isAtom >>= \(AtomTok c) -> return c
  where
    isAtom (AtomTok c) = True
    isAtom _ = False

matchAny :: Parser RegexTok
matchAny = satisfyTok isAny
  where
    isAny AnyTok = True
    isAny _ = False

matchNot :: Parser RegexTok
matchNot = satisfyTok isNot
  where
    isNot NotTok = True
    isNot _ = False

matchAsterate :: Parser RegexTok
matchAsterate = satisfyTok isAsterate
  where
    isAsterate AsterateTok = True
    isAsterate _ = False

matchUnion :: Parser RegexTok
matchUnion = satisfyTok isUnion
  where
    isUnion UnionTok = True
    isUnion _ = False

matchParenthesesOpen :: Parser RegexTok
matchParenthesesOpen = satisfyTok isParenthesesOpen
  where
    isParenthesesOpen ParenthesesOpenTok = True
    isParenthesesOpen _ = False

matchParenthesesClose :: Parser RegexTok
matchParenthesesClose = satisfyTok isParenthesesClose
  where
    isParenthesesClose ParenthesesCloseTok = True
    isParenthesesClose _ = False




  --
