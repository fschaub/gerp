{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module Regex.Parser
  ( runParser
  ) where

import Imports
import Regex.Types
import Regex.Tokenizer

import qualified Text.Parsec as Parsec


runParser :: [RegexTok] -> Either String Regex
runParser = fromError . Parsec.runParser parser () "Parser"
  where
    fromError (Left e) = Left $ show e
    fromError (Right x) = Right x



parser :: Parser Regex
parser = (pNot <|> pAsterate <|> pUnion <|> pAtom) >>= parseSequence
  where
    pAtom = parseAtom <|> parseAny <|> parseParentheses parser
    pNot = parseNot <*> pAtom
    pAsterate = Parsec.try $ pAtom >>= parseAsterate
    pUnion = parseUnion pAtom (pUnion <|> pAtom)
    parseSequence x = Parsec.try (Sequence x <$> parser) <|> pure x


parseAtom :: Parser Regex
parseAtom = Parsec.try $ Atom <$> matchAtom

parseAny :: Parser Regex
parseAny = Parsec.try $ matchAny >> return Any

parseNot :: Parser (Regex -> Regex)
parseNot = Parsec.try $ matchNot >> return Not

parseUnion :: Parser Regex -> Parser Regex -> Parser Regex
parseUnion f g = Parsec.try $ do
  r1 <- f
  matchUnion
  Union r1 <$> g

parseAsterate :: Regex -> Parser Regex
parseAsterate r = Parsec.try $ matchAsterate >> return (Asterate r)

parseParentheses :: Parser Regex -> Parser Regex
parseParentheses p = Parsec.try $ inparents p
  where
    inparents = matchParenthesesOpen `Parsec.between` matchParenthesesClose








--
