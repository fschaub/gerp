module Automata.Alphabet
    ( ASCII(..)
    , AB(..)
    ) where

import Automata.Imports


newtype ASCII = ASCII Char
  deriving (Eq)
instance Show ASCII where
  show (ASCII c) = [c]
instance FromChar ASCII where
  fromChar = ASCII

instance Enum ASCII where
  fromEnum (ASCII c) = fromEnum c - 32
  toEnum i
    | i < 0 || i > 94 = error "enum out of ascii range"
    | otherwise = ASCII $ toEnum (i + 32)
instance Bounded ASCII where
  minBound = toEnum 0
  maxBound = toEnum 94


data AB = A | B
  deriving (Eq)
instance Show AB where
  show A = "a"
  show B = "b"
instance FromChar AB where
  fromChar 'a' = A
  fromChar 'b' = B
  fromChar _ = error "character not allowed for alphabet {a,b}"

instance Enum AB where
  fromEnum A = 0
  fromEnum B = 1
  toEnum 0 = A
  toEnum 1 = B
  toEnum _ = error "enum out of range"
instance Bounded AB where
  minBound = A
  maxBound = B
