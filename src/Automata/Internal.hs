{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Automata.Internal
    ( NFA.NFA
    , DFA.DFA
    , fromDFA
    , fromNFA
    , minimize
    , subsetConstruction
    , module Automata.Imports
    ) where


import qualified Automata.DFA.Internal as DFA
import qualified Automata.DFA.Types as DFA
import qualified Automata.NFA.Internal as NFA
import qualified Automata.NFA.Types as NFA
import qualified Regex as Regex
import qualified Map as Map
import Automata.Imports
import Data.List (sort)

import GHC.Stack
import System.IO
import System.IO.Unsafe

fromNFA :: (Alphabet a, StateLabel b) => NFA.NFA a b -> DFA.DFA a b
fromNFA = minimize . subsetConstruction

fromDFA :: (Alphabet a, StateLabel b) => DFA.DFA a b -> NFA.NFA a b
fromDFA dfa = NFA.NFA
  { NFA.states = DFA.states dfa
  , NFA.initialState = DFA.initialState dfa
  , NFA.delta = \x y -> pure $ DFA.delta dfa x y
  }

-- somehow minimization does not return
minimize ::(Alphabet a, StateLabel b) => DFA.DFA a b -> DFA.DFA a b
minimize dfa = const dfa DFA.DFA
    { DFA.states = minStates
    , DFA.initialState = minInitialState
    , DFA.delta = minDelta
    }
    where
      states = DFA.states dfa
      oldInitialState = DFA.initialState dfa
      minStates = unique $ Map.elems eqMap
      minInitialState = eqMap Map.! oldInitialState
      minDelta s = (eqMap Map.!) . DFA.delta dfa s
      eqMap =
        let distStatesMap = tableFill initialDistinguished
            partition f = foldl' (\(xs,ys) x -> if f x then (x:xs,ys) else (xs,x:ys)) mempty
            eqOf x = partition (\y -> not $ isDistinguished [x,y] distStatesMap)
            mkEqClasses [] = []
            mkEqClasses (x:xs) =
              let (eqs, ys) = eqOf x xs
                  zs = if length ys == length xs then pure <$> ys else mkEqClasses ys
              in eqs : zs
            eqClasses = mkEqClasses states
            renamed = concatMap (\ec -> map (\x -> (x, minimum ec)) ec) eqClasses
            eqMap' = Map.fromList renamed
        in eqMap'
      initialDistinguished =
        let finals = filter isFinal states
            nonfinals = filter (not . isFinal) states
        in Map.fromList $ zip [ sort [x,y] | x <- finals, y <- nonfinals ] $ repeat ()
      isDistinguished xy m = isJust $ m Map.!? sort xy
      unique = sort . foldl' (\acc x -> if x `elem` acc then acc else x:acc) mempty
      tableFill distinguishedMap =
        let dist' = [ sort [x,y]  | x <- states
                                  , y <- states
                                  , let dX = DFA.delta dfa x
                                  , let dY = DFA.delta dfa y
                                  , any (\a -> isDistinguished [dX a, dY a] distinguishedMap) [minBound .. maxBound]
                                  ]
        in
          if null dist' then distinguishedMap else
            tableFill $ distinguishedMap <> Map.fromList (zip dist' $ repeat ())



subsetConstruction :: (HasCallStack, Alphabet a, StateLabel b) => NFA.NFA a b -> DFA.DFA a b
subsetConstruction nfa =
  let initS = pure $ NFA.initialState nfa
      newdeltamap = computeSubsets (Map.singleton [] (const [])) initS
      ss = Map.keys newdeltamap
      renamedstates = setStateLabels [0..] $ map powerState ss
      sMap = Map.fromList $ zip ss renamedstates
      -- invSMap = Map.fromList $ zip renamedstates ss
      ss' = Map.elems sMap
      newdeltamap' = Map.mapKeys (sMap Map.!) . Map.map ((sMap Map.!) .) $ newdeltamap
  in DFA.DFA
    { DFA.states = ss'
    , DFA.initialState = sMap Map.! initS
    , DFA.delta = (Map.!) newdeltamap'
    }
  where
    --computeSubsets :: Map.Map [State b] (a -> [State b]) -> [State b] -> ([[State b]], Map.Map [State b] (a -> [State b]))
    computeSubsets m xs =
      let unique = sort . foldl' (\acc x -> if x `elem` acc then acc else x:acc) []
          uxs =  unique xs
      in if uxs `Map.member` m then m
      else
        let mnew = Map.insert uxs (deltas uxs) m
            deltas ys a = unique $ concatMap (\x -> NFA.delta nfa x a) ys
            yss = [deltas uxs a | a <- [minBound .. maxBound]]
            mres = foldl' (\m y -> m <> computeSubsets m y) mnew yss
        in mres




dfaFromRegex :: (Alphabet a, StateLabel b) => Regex.Regex -> DFA.DFA a b
dfaFromRegex (Regex.Atom c)       = DFA.singleton $ fromChar c
dfaFromRegex (Regex.Any)          = DFA.constTrue
dfaFromRegex (Regex.Asterate r)   = fromNFA . NFA.asterate $ nfaFromRegex r
dfaFromRegex (Regex.Not r)        = DFA.neg $ dfaFromRegex r
dfaFromRegex (Regex.Sequence r1 r2)    = fromNFA $ NFA.concatenate (nfaFromRegex r1) (nfaFromRegex r2)
dfaFromRegex (Regex.Union r1 r2)  = fromNFA $ NFA.union (nfaFromRegex r1) (nfaFromRegex r2)

nfaFromRegex :: (Alphabet a, StateLabel b) =>  Regex.Regex -> NFA.NFA a b
nfaFromRegex (Regex.Atom c)       = NFA.singleton $ fromChar c
nfaFromRegex (Regex.Any)          = NFA.constTrue
nfaFromRegex (Regex.Asterate r)   = NFA.asterate $ nfaFromRegex r
nfaFromRegex (Regex.Not r)        = fromDFA . DFA.neg $ dfaFromRegex r
nfaFromRegex (Regex.Sequence r1 r2)    = NFA.concatenate (nfaFromRegex r1) (nfaFromRegex r2)
nfaFromRegex (Regex.Union r1 r2)  = NFA.union (nfaFromRegex r1) (nfaFromRegex r2)




instance (Alphabet a, StateLabel b) => Automata DFA.DFA a b where
  accepts = DFA.dfaaccepts
  labmap = DFA.dfalabmap
  alphamap = DFA.dfaalphamap
  fromRegex = dfaFromRegex
  renameStates = DFA.dfarenameStates

instance (Alphabet a, StateLabel b) => Automata NFA.NFA a b where
  accepts = NFA.nfaaccepts
  labmap = NFA.nfalabmap
  alphamap = NFA.nfaalphamap
  fromRegex = nfaFromRegex
  renameStates = NFA.nfarenameStates
