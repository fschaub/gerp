{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}


module Automata.DFA.Internal
    ( compute
    , neg
    , singleton
    , constTrue
    , dfaaccepts
    , dfaalphamap
    , dfalabmap
    , dfarenameStates
    ) where

import GHC.Stack

import Automata.Imports
import Automata.DFA.Types
import qualified Regex
import qualified Data.Map as Map

dfaaccepts :: (Automata DFA a b) => DFA a b -> [a] -> Bool
dfaaccepts dfa = isFinal . compute dfa (initialState dfa)

dfaalphamap :: (Automata DFA a b, Automata DFA c b) => (c -> a) -> DFA a b -> DFA c b
dfaalphamap f dfa = DFA
  { states = states dfa
  , initialState = initialState dfa
  , delta = \x y -> delta dfa x $ f y
  }

dfalabmap :: (Automata DFA a b, Automata DFA a d) => (b -> d) -> DFA a b -> DFA a d
dfalabmap g dfa = DFA
  { states = newStates
  , initialState = g <$> initialState dfa
  , delta = \x y -> g <$> delta dfa (invg x) y
  }
  where
    invg = (Map.!) $ Map.fromList $ zip newStates oldStates
    oldStates = states dfa
    newStates = map (g <$>) oldStates

dfarenameStates :: (HasCallStack, Automata DFA a b, StateLabel d)
  => [(b,d)] -> DFA a b -> DFA a d
dfarenameStates m dfa = DFA
  { states = newStates
  , initialState = newInitState
  , delta = newDelta
  }
  where
    labMap = Map.fromList m
    invLabMap = Map.fromList $ map (\(x,y) -> (y,x)) m
    newStates = fmap (labMap Map.!) <$> states dfa
    newInitState = (labMap Map.!) <$> initialState dfa
    newDelta x y = (labMap Map.!) <$> delta dfa ((invLabMap Map.!) <$> x) y



compute :: (Automata DFA a b) => DFA a b -> State b -> [a] -> State b
compute dfa = foldl' (delta dfa)


neg :: (Automata DFA a b) => DFA a b -> DFA a b
neg dfa = dfa
  { states = map toggleState $ states dfa
  , initialState = toggleState $ initialState dfa
  , delta = \x -> toggleState . delta dfa (toggleState x)
  }

singleton :: Automata DFA a b => a -> DFA a b
singleton c = DFA
  { states = sstates
  , initialState = istate
  , delta = dfun
  }
  where
    sstates = [istate, fstate, dstate]
    fstate = FinalState $ toEnum 1
    dstate = State $ toEnum 2
    istate = State $ toEnum 0
    dfun s x
      | s == istate && x == c = fstate
      | s == istate && x /= c = dstate
      | otherwise = dstate

constTrue :: Automata DFA a b => DFA a b
constTrue = DFA
  { states = sstates
  , initialState = istate
  , delta = dfun
  }
  where
    sstates = [istate, fstate, dstate]
    fstate = FinalState $ toEnum 1
    dstate = State $ toEnum 2
    istate = State $ toEnum 0
    dfun s _
      | s == istate = fstate
      | s == fstate = dstate
      | otherwise = dstate


--
