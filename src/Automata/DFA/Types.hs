module Automata.DFA.Types
    ( DFA(..)
    ) where

import Automata.Imports

data DFA a b = DFA
  { states :: [State b]
  , initialState :: State b
  , delta :: State b -> a -> State b
  }
