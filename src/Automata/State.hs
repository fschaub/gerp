module Automata.State
    ( isFinal
    , finalStates
    , asFinal
    , asNonFinal
    , powerState
    , toggleState
    , stateLabel
    , setStateLabels
    ) where

import Automata.Types


powerState :: [State a] -> State [State a]
powerState xs
  | any isFinal xs = FinalState xs
  | otherwise = State xs


isFinal :: State a -> Bool
isFinal (FinalState _) = True
isFinal _ = False

finalStates :: StateLabel a => [State a] -> [State a]
finalStates = filter isFinal

asFinal :: StateLabel a => State a -> State a
asFinal (State a) = FinalState a
asFinal x = x

asNonFinal :: StateLabel a => State a -> State a
asNonFinal (FinalState a) = State a
asNonFinal x = x

toggleState :: StateLabel a => State a -> State a
toggleState s
  | isFinal s = asNonFinal s
  | otherwise = asFinal s

stateLabel :: State a -> a
stateLabel (FinalState x) = x
stateLabel (State x) = x

setStateLabels :: (StateLabel c) => [Int] -> [State b] -> [State c]
setStateLabels = zipWith (fmap . const . toEnum)


setLabels :: (StateLabel c) => [Int] -> [b] -> [c]
setLabels = zipWith (const . toEnum)
