{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Automata.NFA.Types
    ( NFA(..)
    ) where


import Automata.Imports
import qualified Data.Map as Map

data NFA a b = NFA
  { states :: [State b]
  , initialState :: State b
  , delta :: State b -> a -> [State b]
  }
