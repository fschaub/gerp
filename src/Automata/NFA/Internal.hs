{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}



module Automata.NFA.Internal
    ( compute
    , deltaStep
    , union
    , intersection
    , asterate
    , concatenate
    , singleton
    , constTrue
    , nfaaccepts
    , nfaalphamap
    , nfalabmap
    , nfarenameStates
    ) where

import GHC.Stack

import Automata.Imports
import Automata.NFA.Types
import qualified Data.Bifunctor as Bifunctor
import qualified Map as Map

nfaaccepts :: (Automata NFA a b) => NFA a b -> [a] -> Bool
nfaaccepts nfa = any isFinal . compute nfa (initialState nfa)

nfaalphamap :: (Automata NFA a b, Automata NFA c b) => (c -> a) -> NFA a b -> NFA c b
nfaalphamap f nfa = NFA
  { states = states nfa
  , initialState = initialState nfa
  , delta = \x y -> delta nfa x $ f y
  }

nfalabmap :: (HasCallStack, Automata NFA a b, Automata NFA a d) => (b -> d) -> NFA a b -> NFA a d
nfalabmap g nfa = NFA
  { states = newStates
  , initialState = g <$> initialState nfa
  , delta = newDelta
  }
  where
    invg = Map.fromList $ zip newStates oldStates
    oldStates = states nfa
    newStates = map (g <$>) oldStates
    newDelta x y
      | x `Map.member` invg = map (g <$>) $ delta nfa ((Map.!) invg x) y
      | otherwise = []

nfarenameStates :: (HasCallStack, Automata NFA a b, StateLabel d)
  => [(b,d)] -> NFA a b -> NFA a d
nfarenameStates m nfa = NFA
  { states = newStates
  , initialState = newInitState
  , delta = newDelta
  }
  where
    labMap
      | map stateLabel (states nfa) /= map fst m = error "cannot apply partial rename of states"
      | otherwise = Map.fromList m
    invLabMap = Map.fromList $ map (\(x,y) -> (y,x)) m
    newStates = fmap (labMap Map.!) <$> states nfa
    newInitState = (labMap Map.!) <$> initialState nfa
    newDelta x y
      | stateLabel x `Map.member` invLabMap = fmap (labMap Map.!) <$> delta nfa ((invLabMap Map.!) <$> x) y
      | otherwise = mempty

compute :: (Automata NFA a b) => NFA a b -> State b -> [a] -> [State b]
compute nfa state = foldl' (deltaStep nfa) [state]

deltaStep :: (Automata NFA a b) => NFA a b -> [State b] -> a -> [State b]
deltaStep nfa states x = unique . concatMap (\n -> delta nfa n x) $ states
  where
    unique = foldl' (\acc x -> if x `elem` acc then acc else x:acc) []

uniqueNFAs :: (HasCallStack, Automata NFA a b) => NFA a b -> NFA a b -> (NFA a b, NFA a b)
uniqueNFAs a b = (ua, ub)
  where
    -- rename states of both NFAs
    aLabs = map stateLabel $ states a
    aNewLabs = map toEnum [0 ..]
    bLabs = map stateLabel $ states b
    bNewLabs = map toEnum [length aLabs ..]
    ua = nfarenameStates (zip aLabs aNewLabs) a
    ub = nfarenameStates (zip bLabs bNewLabs) b



union :: (HasCallStack, Automata NFA a c) => NFA a c -> NFA a c -> NFA a c
union nfaA nfaB = NFA
  { states = unionStates
  , initialState = unionInitialState
  , delta = unionDelta
  }
  where
    -- rename states of both NFAs
    uniques = uniqueNFAs nfaA nfaB
    uniqueA = fst uniques
    uniqueB = snd uniques
    -- new initial state (e-transition to old ones)
    unionInitialState = State . toEnum . (+1) $ length (states uniqueA) + length (states uniqueA)
    -- union the unique states
    unionStates = unionInitialState : states uniqueA ++ states uniqueB
    -- union of the delta functions
    unionDelta' x y = delta uniqueA x y ++ delta uniqueB x y
    initDelta x y
      | x == unionInitialState = concatMap (flip unionDelta' y . initialState) [uniqueA, uniqueB]
      | otherwise = []
    unionDelta x y = initDelta x y ++ unionDelta' x y

intersection :: (HasCallStack, Automata NFA a c) => NFA a c -> NFA a c -> NFA a c
intersection nfaA nfaB = minimize NFA
  { states = intersectionStates
  , initialState = intersectionInitialState
  , delta = intersectionDelta
  }
  where
    -- new States
    intersectStates x y
      | isFinal x && isFinal y = FinalState (stateLabel x, stateLabel y)
      | otherwise = State (stateLabel x, stateLabel y)
    stateTuples = [ (x,y) | x <- states nfaA, y <- states nfaB ]
    tupleStates = [ intersectStates x y | (x,y) <- stateTuples ]
    transMap = Map.fromList $ zip stateTuples intersectionStates
    invTransMap = Map.fromList $ zip intersectionStates stateTuples
    intersectionStates = setStateLabels [0..] tupleStates
    -- initialState
    intersectionInitialState = transMap Map.! (initialState nfaA, initialState nfaB)
    -- deltafunction
    intersectionDelta s x =
      let (as', bs') = invTransMap Map.! s
          as = delta nfaA as' x
          bs = delta nfaB bs' x
      in [ transMap Map.! (a,b) | a <- as, b <- bs ]
    -- remove unreachable states
    minimize nfa =
      let ss = states nfa
          alpha = [minBound .. maxBound]
          resStates = concat [ delta nfa s a | s <- ss, a <- alpha ]
          newStates = foldl' (\ss s -> if s `elem` ss then ss else s:ss) [] resStates
      in nfa { states = newStates }


asterate :: (HasCallStack, Automata NFA a b) => NFA a b -> NFA a b
asterate nfa = nfa
  { states = newStates
  , initialState = newInitState
  , delta = adelta
  }
  where
    newInitState = asFinal $ initialState nfa
    newStates = map changeInitState $ states nfa
    changeInitState x
      | x == initialState nfa = newInitState
      | otherwise = x
    toOriginalInitState x
      | x == newInitState = initialState nfa
      | otherwise = x
    adelta x = addInitState . map changeInitState . delta nfa (toOriginalInitState x)
    addInitState xs
      | newInitState `elem` xs = xs
      | any isFinal xs = newInitState : xs
      | otherwise = xs


concatenate :: (HasCallStack, Automata NFA a b) => NFA a b -> NFA a b -> NFA a b
concatenate nfaA nfaB = NFA
  { states = concStates
  , initialState = concInitialState
  , delta = concDelta
  }
  where
    -- rename states of both NFAs
    uniques = uniqueNFAs nfaA nfaB
    uniqueA = fst uniques
    uniqueB = snd uniques
    -- new states
    newAStates = map (\x -> if isFinal x then toggleState x else x) $ states uniqueA
    aStatesMap = Map.fromList $ zip (states uniqueA) newAStates
    invAStatesMap = Map.fromList $ zip newAStates (states uniqueA)
    newAFinalStates = Map.keys $ Map.filter isFinal invAStatesMap
    concStates = newAStates ++ states uniqueB
    -- new initial state
    concInitialState = aStatesMap Map.! initialState uniqueA
    -- new delta
    newDeltaA x
      | x `Map.member` invAStatesMap = addB . fmap (aStatesMap Map.!) . delta uniqueA (invAStatesMap Map.! x)
      | otherwise = mempty
    addB xs
      | any (`elem` newAFinalStates) xs  = initialState uniqueB : xs
      | otherwise = xs
    concDelta x y = unique $ newDeltaA x y ++ delta uniqueB x y
    unique = foldl' (\acc x -> if x `elem` acc then acc else x:acc) []


singleton :: Automata NFA a b => a -> NFA a b
singleton c = NFA
  { states = sstates
  , initialState = istate
  , delta = dfun
  }
  where
    sstates = [istate, fstate]
    fstate = FinalState $ toEnum 1
    istate = State $ toEnum 0
    dfun s x
      | s == istate && x == c = [fstate]
      | otherwise = []


constTrue :: Automata NFA a b => NFA a b
constTrue = NFA
  { states = sstates
  , initialState = istate
  , delta = dfun
  }
  where
    sstates = [istate, fstate]
    fstate = FinalState $ toEnum 1
    istate = State $ toEnum 0
    dfun s _
      | s == istate = [fstate]
      | otherwise = []




--
