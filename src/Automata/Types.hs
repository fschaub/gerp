{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Automata.Types
    ( State(..)
    , Alphabet
    , FromChar(..)
    , StateLabel
    , Automata(..)
    ) where

import qualified Regex

type Alphabet a = (Eq a, Show a, FromChar a, Bounded a, Enum a)
type StateLabel a = (Eq a, Show a, Enum a, Ord a)

class FromChar a where
  fromChar :: Char -> a
  pack :: String -> [a]
  pack = map fromChar


data State a = State a | FinalState a
  deriving (Show, Eq, Ord)
instance Functor State where
  fmap f (FinalState x) = FinalState $ f x
  fmap f (State x) = State $ f x

class (StateLabel b, Alphabet a) => Automata (m :: * -> * -> *) a b where
  accepts :: m a b -> [a] -> Bool
  fromRegex :: Regex.Regex -> m a b
  labmap :: (StateLabel d) => (b -> d) -> m a b -> m a d
  renameStates :: (StateLabel d) => [(b,d)] -> m a b -> m a d
  alphamap :: (Alphabet c) => (c -> a) -> m a b -> m c b
  amap :: (Automata m a d, Automata m c d) => (c -> a) -> (b -> d) -> m a b -> m c d
  amap f g = alphamap f . labmap g
