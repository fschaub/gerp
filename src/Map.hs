module Map
    ( module OrigMap
    , (!)
    ) where

import Data.Map as OrigMap hiding ((!))
import qualified Data.Map as RealMap
import GHC.Stack



(!) :: (Ord k, Show k, HasCallStack) => Map k v -> k -> v
(!) m k
  | k `member` m = m RealMap.! k
  | otherwise = error
      $ "not elem of map: " ++ show k ++ "\n"
      ++ prettyCallStack callStack ++ "\n\n"
      ++ show (RealMap.keys m)
