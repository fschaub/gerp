{-# LANGUAGE DataKinds #-}

module Options
  ( Args(..)
  , runCommand
  ) where

import Imports
import qualified Options.Declarative as Opt

data Args = Args
  { regex :: String
  } deriving (Show)

getArgs :: Opt.Arg "REGEX" String
        -> Opt.Cmd "match strings from StdIn" Args
getArgs re = return $ Args { regex = Opt.get re }


runCommand :: (Args -> IO ()) -> IO ()
runCommand f = Opt.run_ $ getArgs >=> void . liftIO . f
