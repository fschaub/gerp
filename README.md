# gerp - a grep clone

## building

use `stack build` to build and `stack run` to run the program.
if you encounter missing dependencies make sure the following are available
```yaml
- parsec >= 3.1 && < 3.2
- optparse-declarative >= 0.4 && < 0.5
- containers
```


## running

the application expects a regular expression as argument and some stdin to match on. the standard format of the expression is

```haskell
defaultTokens :: Tokens
defaultTokens = Tokens
  { escapeChar = '\\'
  , notsymbolChar = '^'
  , asterateChar = '*'
  , anysymbolChar = '.'
  , unionChar = '+'
  , parenthesesOpenChar = '('
  , parenthesesCloseChar = ')'
  }
```
you can escape these characters with the given escape char (default is '\\'). If you prefer different settings, edit the tokens in `Regex.Tokenizer`.

__NOTE__: in contrast to the real grep we do not add '.*' before and after your expression automatically. You have to match the whole line with your expression.


----------------------------------------
Author: Fabian Schaub
Licence: GPLv3

available at [gitlab.com/fschaub/gerp.git](https://gitlab.com/fschaub/gerp.git)
